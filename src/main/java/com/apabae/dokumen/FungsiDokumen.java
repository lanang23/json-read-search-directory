package com.apabae.dokumen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;

public class FungsiDokumen {
	
	private List<JSONDatum> searchData = new ArrayList<JSONDatum>();
	
	public List<JSONDatum> getSearchData() {
		return searchData;
	}
	
	public void findFile(String name, File file, String parentDir) {
		name = name.toLowerCase();
        File[] list = file.listFiles();
        String excludeType = getExcludedType();
        
        if(list!=null)
        for (File fileEntry : list) {
        	//System.out.println(fileEntry.getPath());
            if (fileEntry.getName().toLowerCase().matches("(.*)"+name+"(.*)")) {	
            	String fullPath = fileEntry.getPath().replaceAll("^"+parentDir, "");
            	JSONDatum datum = trimDatum(fileEntry, fullPath, excludeType);
    			
    			if (datum != null) {
    				searchData.add(datum);
    			}
            }
            
            if (fileEntry.isDirectory())
            {
                findFile(name, fileEntry, parentDir);
            }
        }
    }
	
	public List<JSONDatum> readDir(String topDir, String subDir, String path) {
		List<JSONDatum> data = new ArrayList<JSONDatum>();
		
		String dir = topDir + subDir;
		File folder = new File(dir + path);
		String excludeType = getExcludedType();
		
		for (File fileEntry: folder.listFiles()) {
			String fullPath = path + '/' + fileEntry.getName();
			JSONDatum datum = trimDatum(fileEntry, fullPath, excludeType);
			
			if (datum != null) {
				data.add(datum);
			}
		}
		
		return data;
	}
	
	private JSONDatum trimDatum(File fileEntry, String fullPath, String excludeType) {
		boolean isDir = false;
		
		// Kalau Dir statusnya true
		if (fileEntry.isDirectory()) {
			isDir = true;
		}
		
		byte[] bytesEncoded = Base64.encodeBase64(fullPath.getBytes());
		
		// pathnya di-encode jadi base64 biar delimiter pathnya ga mengganggu url
		String pathBase64 = new String(bytesEncoded);
		String filename = fileEntry.getName();
		String ext = getExt(filename);
		
		// Kalau ditemukan ekstensi ini maka jangan tampilkan
		if (ext.matches("("+excludeType+")$") || filename.substring(0, 1).equals("~") ) {
			return null;
		}
		
		JSONDatum datum = new JSONDatum(filename, pathBase64, isDir, ext);
		
		return datum;
	}
	
	public boolean isDir(String topDir, String subDir, String openPath) {
		String fullPath = topDir + subDir + openPath;
		File check = new File(fullPath);
		
		if (check.isDirectory())
			return true;
		
		return false;
	}
	
	public boolean isExists(String topDir, String subDir, String openPath) {
		String fullPath = topDir + subDir + openPath;
		File check = new File(fullPath);
		
		if (check.exists())
			return true;
		
		return false;
	}
	
	private String getExt(String path) {
		String[] cut = path.split("\\.");
		
		if (cut.length > 1)
			return cut[cut.length - 1];
		else
			return "";
	}
	
	private String getExcludedType() {
		ConfigFile config = new ConfigFile();
		
		try (BufferedReader br = new BufferedReader(new FileReader(config.getDokumenExcludeFile()))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				return sCurrentLine;
			}

		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("tidak ada ditemukan file di: "+config.getDokumenExcludeFile());
		}
		
		return "exe|msi|lnk";
	}
}
