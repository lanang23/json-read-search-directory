package com.apabae.dokumen;

public class JSONDatum {
	private final String filename;
	private final String path;
	private final boolean isDir;
	private final String ext;
	
	public String getFilename() {
		return filename;
	}

	public String getPath() {
		return path;
	}

	public boolean getisDir() {
		return isDir;
	}

	public String getExt() {
		return ext;
	}

	public JSONDatum(String filename, String path, boolean isDir, String ext) {
		this.filename = filename;
		this.path = path;
		this.isDir = isDir;
		this.ext = ext;
	}
}
