package com.apabae.dokumen;

import java.util.List;

public class JSONData {
	private final String top;
	private final boolean error;
	private final String msg;
	private List<JSONDatum> datum;
	
	public String getTop() {
		return top;
	}
	
	public boolean getError() {
		return error;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public List<JSONDatum> getData() {
		return datum;
	}
	
	public JSONData(boolean error, String msg, String top) {
		this.error = error;
		this.msg = msg;
		this.top = top;
	}
	
	public JSONData(boolean error, String msg, String top, List<JSONDatum> datum) {
		this.error = error;
		this.msg = msg;
		this.top = top;
		this.datum = datum;
	}
}
