package com.apabae.dokumen;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
public class MainController {
	// ini untuk membatasi folder biar ga bisa masuk ke folder di atasnya
	String topDir = new ConfigFile().getTopDir();
			
	@RequestMapping("/")
    //@ResponseBody
    String home() {
        return "index";
    }
	
	@RequestMapping(value={"/dir/{subDir}/{path}", "/dir/{subDir}"})
	@ResponseBody
	JSONData dir(@PathVariable String subDir, @PathVariable Optional<String> path) {
		
		// ini untuk dir di bawah top dir
		//String subDir = "BacaFolder/";
		
		String openPath = "";
		String top = "";
		
		if (path.isPresent()) {
			byte[] bytesDecoded = Base64.decodeBase64(path.get());
			openPath = new String(bytesDecoded);

			String[] cut = openPath.split("/");
			String parent = "";
			
			for (int i = 0; i<cut.length; i++) {
				if (i < cut.length - 1) {
					String part =  cut[i];
					parent = parent + "/" + part;
				}
			}
			
			parent = parent.replaceAll("/$", "").replaceAll("^/", "");
			
			// Kalau top hanya / jangan dimasukkan
			if (! parent.equals("/") && parent.isEmpty() == false) {
				top = new String(Base64.encodeBase64(parent.getBytes()));
				System.out.println("Masuk atas");
				System.out.println(parent);
			}
			else if (openPath.isEmpty() == false && openPath.equals("/") == false) {
				top = new String(Base64.encodeBase64("/".getBytes()));
				System.out.println("Masuk bawah");
			}			
		}
		
		FungsiDokumen fungsiDokumen = new FungsiDokumen();
		
		// Kalau Dokumen ini ga ada atau ini bukan folder
		if (false == fungsiDokumen.isExists(topDir, subDir, openPath) || 
			false == fungsiDokumen.isDir(topDir, subDir, openPath))
		{
			return new JSONData(false, "", "");
		}
		
		return new JSONData(false, "", top, fungsiDokumen.readDir(topDir, subDir, openPath));
	}
	
	@RequestMapping("/read/{subDir}/{path}/{filename:.+}")
	@ResponseBody
	public void read(HttpServletResponse response, @PathVariable String subDir, @PathVariable String path, @PathVariable String filename) throws IOException {
		// Karena pathnya base64 maka harus didecode dulu
		byte[] bytesDecoded = Base64.decodeBase64(path);
		String openPath = new String(bytesDecoded);
		String fullPath = topDir + subDir + openPath;
		
		Path getPath = Paths.get(fullPath);
		byte[] data = Files.readAllBytes(getPath);
		String mime = Files.probeContentType(getPath);
		
		if (filename.trim().equals(getPath.getFileName().toFile().getName()))
		{
			response.setContentType(mime);
	        
			//response.setHeader("Content-disposition", "attachment; filename=\"" + getPath.getFileName() + "\"");
	        response.setContentLength(data.length);
	       	response.getOutputStream().write(data);
	        response.getOutputStream().flush();
		}
	}
	
	@RequestMapping(value={"/search/{subDir}/{query}", "/search/{subDir}"})
	@ResponseBody
	JSONData search(@PathVariable String subDir, @PathVariable Optional<String> query) {
		String queryText = "";
		String parentDir = topDir + subDir;
		File file = new File(parentDir);

		if (query.isPresent()) {
			queryText = query.get();
		}
		
		FungsiDokumen fungsiDokumen = new FungsiDokumen();
		
		if (queryText.trim().isEmpty() == false) {
			fungsiDokumen.findFile(queryText.trim(), file, parentDir);
		}
		
		byte[] encoded = Base64.encodeBase64("/".getBytes());
		String top = new String(encoded);
		
		return new JSONData(false, "", top, fungsiDokumen.getSearchData());
	}
}