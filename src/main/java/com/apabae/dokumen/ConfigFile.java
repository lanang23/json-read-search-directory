package com.apabae.dokumen;

public class ConfigFile {
	
	private String topDir = "/opt/dokumen/";
	private String dokumenExcludeFile = "/opt/dokumen.exclude";
	
	public String getTopDir() {
		return this.topDir;
	}

	public String getDokumenExcludeFile() {
		return dokumenExcludeFile;
	}
}
